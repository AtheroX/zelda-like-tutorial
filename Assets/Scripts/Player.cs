﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Player : MonoBehaviour {

	public float speed = 4f;
	public GameObject SpawnMap;

	private Animator anim;
	private Rigidbody2D rb2d;
	private Vector2 mov;

	void Awake () {
		Assert.IsNotNull(SpawnMap);
	}

	void Start () {
		anim = GetComponent<Animator>();
		rb2d = GetComponent<Rigidbody2D>();

		Camera.main.GetComponent<MainCamera>().Limites(SpawnMap);
	}

	void Update () {

		mov = new Vector2(
			Input.GetAxisRaw("Horizontal"),
			Input.GetAxisRaw("Vertical")
		);

		if (mov != Vector2.zero) {
			anim.SetFloat("MovX", mov.x);
			anim.SetFloat("MovY", mov.y);
			anim.SetBool("walking", true);
		} else {
			anim.SetBool("walking", false);
		}

	}

	void FixedUpdate () {
		
		rb2d.MovePosition(rb2d.position + mov * speed * Time.deltaTime);
	}
}