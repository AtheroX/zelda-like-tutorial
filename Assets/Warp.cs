﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Warp : MonoBehaviour {

	public GameObject target;
	public GameObject TargetMap;


	void Awake (){

		Assert.IsNotNull (target);
		Assert.IsNotNull (TargetMap);


		GetComponent<SpriteRenderer> ().enabled = false;
		transform.GetChild(0).GetComponent<SpriteRenderer> ().enabled = false;

	}

	void Start () {
		
	}
	
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other){

		other.transform.position = target.transform.GetChild (0).transform.position;
		Camera.main.GetComponent<MainCamera>().Limites(TargetMap);

	}
}
